/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"

//--------------------------------------------------------------------------
//                           Class Robot11
//--------------------------------------------------------------------------

class Robot11 : public Driver
{
public:
    // Konstruktor
    Robot11()
    {
        // Der Name des Robots
        m_sName = "Robot11";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    con_vec drive(situation& s)
    {
        // hier wird die Logik des Robots implementiert
        con_vec result = CON_VEC_EMPTY;
        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot11Instance()
{
    return new Robot11();
}
