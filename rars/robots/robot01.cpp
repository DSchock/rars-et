//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "car.h"

//--------------------------------------------------------------------------
//                           D E F I N E S
//--------------------------------------------------------------------------

// parameters to tinker with:
const double SPURWECHSEL = 1.0;        // when car "dead_ahead, change "lane" this much
const double FLIEHKRAFT   =  .92;       // lateral g's expected when cornering
const double BREMSEN_GERADE = -30.0;     // acceleration when braking on straight
const double ABS_FAKTOR = .935;      // tire speed ratio when braking   "
const double BREMSEN_KURVE = -25.0;     // acceleration when braking in curve
const double BRK_CRV_SLIP = 5.0;      // tire slip for braking in curve
const double KURVENABSTAND_INNEN = 12.0; // target distance from curve's inner rail
const double STEER_GAIN = 0.5;        // gain of steering servo loop
const double  DAMP_GAIN = 1.1;        // damping of steering servo loop
const double  BIG_SLIP = 9.0;         // affects the bias of steering servo loop

//--------------------------------------------------------------------------
//                           Class Tutorial3
//--------------------------------------------------------------------------


class Robot01 : public Driver
{
public:
    // Konstruktor
    Robot01()
    {
        // Der Name des Robots
        m_sName = "Robot01";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oMAGENTA;
        m_iTailColor = oMAGENTA;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

      double corn_spd(double radius)     // returns maximum cornering speed, fps
      {
        //  MUST NEVER CALL THIS ROUTINE WITH ZERO ARGUMENT!
        return sqrt(radius * 32.2 * FLIEHKRAFT);     // compute the speed
      }

      // Calculates the critical distance necessary to bring a car from speed
      // v0 to speed v1 when the braking acceleration is "a", ft per sec^2.
      // Speeds are in fps.  ("a" should be negative)
      double CritDist(double v0, double v1, double a)
      {
        double dv;

        dv = v1 - v0;
        if(dv > 0.0)          // this saves having such a test in the caller
          return(0.0);
        return (v0 + .5 * dv) * dv / a;
      }

    con_vec drive(situation& s)
    {
        con_vec result;                    // This is what is returned.
    double winkel, geschwindigkeit;                  // components of result
    static double lane = -10000;    // an absurd value to show not initialized
    double bias, speed, speed_next, width, to_end;

    if( s.starting )
    {
      result.fuel_amount = MAX_FUEL;     // fuel when starting
    }

    // service routine in the host software to handle getting unstuck from
    // from crashes and pileups:
    if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc))
      return result;

    width = s.to_lft + s.to_rgt;   // compute width of track

    // This is a little trick so that the car will not try to change lanes
    // during the "dragout" at the start of the race.  We set "lane" to
    // whatever position we have been placed by the host.
    if(lane < -9000)              // will be true only once
      lane = s.to_lft;           // better not to change lanes at the start

    // Set "lane" during curves.  This robot sets "lane" during curves to
    // try to maintain a fixed distance to the inner rail.
    // For straightaways, we leave "lane" unchanged until later.
    if(s.cur_rad > 0.0)                // turning left
      lane = KURVENABSTAND_INNEN;
    else if(s.cur_rad < 0.0)           // turning right
      lane = width - KURVENABSTAND_INNEN;

    // set the bias:
    // Bias is an additive term in the steering servo, so that the servo
    // doesn't have to "hunt" much for the correct winkel value.  It is an
    // estimate of the winkel value that would be found by the servo if there
    // was plenty of settling time.  It is zero for straightaways.
    // Also, for convenience, we call the corn_spd() function here.  On
    // the straightaway, we call it to find out the correct speed for the
    // corner ahead, using s.nex_rad for the radius.  In the curve we of
    // course use the radius of the curve we are in.  But also, we call it
    // for the next segment, to find out our target speed for the end of
    // the current segment, which we call speed_next.
    if(s.cur_rad == 0.0)
    {
      bias = 0.0;
      if(s.nex_rad > 0.0)
        speed = corn_spd(s.nex_rad + KURVENABSTAND_INNEN);
      else if(s.nex_rad < 0.0)
        speed = corn_spd(-s.nex_rad + KURVENABSTAND_INNEN);
      else
        speed = 250.0;
    }
    else
    {
      if(s.nex_rad == 0.0)
        speed_next = 250.0;
      else
        speed_next = corn_spd(fabs(s.nex_rad) + KURVENABSTAND_INNEN);
      speed = corn_spd(fabs(s.cur_rad) + KURVENABSTAND_INNEN);
      bias = (s.v*s.v/(speed*speed)) * atan(BIG_SLIP / speed);
      if(s.cur_rad < 0.0)   // bias must be negative for right turn
        bias = -bias;
    }

    // set winkel:  (This line is the complete steering servo.)
    winkel = STEER_GAIN * (s.to_lft - lane)/width - DAMP_GAIN * s.vn/s.v + bias;

    // set geschwindigkeit:  When nearing end of straight, change "lane" for the turn, also.
    if(s.cur_rad == 0.0)               // If we are on a straightaway,
    {
                                       // if we are far from the end,
      if(s.to_end > CritDist(s.v, speed, BREMSEN_GERADE))
        geschwindigkeit = s.v + 50.0;               // pedal to the metal!
      else                             // otherwise, adjust speed for the coming turn:
      {
        if(s.v > 1.02 * speed)         // if we're 2% too fast,
          geschwindigkeit = ABS_FAKTOR * s.v;      // brake hard.
        else if(s.v < .98 * speed)     // if we're 2% too slow,
          geschwindigkeit = 1.1 * speed;            // accelerate hard.
        else                           // if we are very close to speed,
          geschwindigkeit = .5 * (s.v + speed);     // approach the speed gently.
        // approach the lane you want for the turn:
        if(s.nex_rad > 0.0)
          lane = KURVENABSTAND_INNEN;
        else
          lane = width - KURVENABSTAND_INNEN;
      }
    }
    else       // This is when we are in a curve:  (seek correct speed)
    {
      // calculate geschwindigkeit to maintain speed in corner
      geschwindigkeit = .5 * (s.v + speed)/cos(winkel);
      // calculate distance to end of curve:
      if(s.cur_rad > 0.0)
        to_end = s.to_end * (s.cur_rad + KURVENABSTAND_INNEN);
      else
        to_end = -s.to_end * (s.cur_rad - KURVENABSTAND_INNEN);
      // compute required braking distance and compare:
      if(to_end <= CritDist(s.v, speed_next, BREMSEN_KURVE))
      {
         geschwindigkeit = s.v - BRK_CRV_SLIP;
      }
    }

    // During the acceleration portion of a straightaway, the lane variable
    // is not changed by the code above.  Hence the code below changes it a
    // little at a time until there is no car dead_ahead.  This code here has
    // no affect at all in the turns, nor in the braking portion
    // of the straight.
    if(s.dead_ahead)                   // Change the lane a little if someone's
    {
        if(s.to_lft > s.to_rgt)          // in your way.
            lane -= SPURWECHSEL;            // lane must be a static variable
        else
            lane += SPURWECHSEL;
    }
    result.vc = geschwindigkeit;   result.alpha = winkel;

    // Pit: if the fuel is too low
    //  Fuel: full
    //  Damage: repair all
    if( s.fuel<10.0 )
    {
      result.request_pit   = 1;
      result.repair_amount = s.damage;
      result.fuel_amount = MAX_FUEL;
    }

    return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot01Instance()
{
    return new Robot01();
}
