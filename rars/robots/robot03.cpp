/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <stdlib.h>
#include <math.h>
#include "car.h"
#include "track.h"

//--------------------------------------------------------------------------
//                           Class Robot03
//--------------------------------------------------------------------------

class Robot03 : public Driver
{
public:
    // Konstruktor
    Robot03()
    {
        // Der Name des Robots
        m_sName = "Robot03";
        // Namen der Autoren
        m_sAuthor = "Gruppe06";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

      double corn_spd(double radius)  // returns maximum cornering speed, fps
      {
        const double CORN_MYU   = 1.00;     // lateral g's expected when cornering

        //  MUST NEVER CALL THIS ROUTINE WITH ZERO OR NEGATIVE ARGUMENT!
        return sqrt(radius * 32.2 * CORN_MYU);     // compute the speed
      }

      // Calculates the critical distance necessary to bring a car from speed
      // v0 to speed v1 when the braking acceleration is "a", ft per sec^2.
      // Speeds are in fps.  ("a" should be negative)
      double CritDist(double v0, double v1, double a)
      {
        double dv;

        dv = v1 - v0;
        if(dv > 0.0)          // this saves having such a test in the caller
          return(0.0);
        return (v0 + .5 * dv) * dv / a;
      }


    con_vec drive(situation& s)
    {

    //--------------------------------------------------------------------------
    //                           D E F I N E S
    //--------------------------------------------------------------------------

    // parameters to tinker with:
    // accelerations are in feet/second per second.
    // slips are in feet/second
    // distances are in feet

    const double BRAKE_ACCEL = -33.0;   // acceleration when braking on straight
    const double BRAKE_SLIP  = 6.5;     // tire slip when braking
    const double BRK_CRV_ACC = -27.0;   // acceleration when braking in curve
    const double BRK_CRV_SLIP = 3.5;    // tire slip for braking in curve
    const double MARGIN = 10.0;         // target distance from curve's inner rail
    const double MARG2 = MARGIN+10.0;   // target for entering the curve
    const double ENT_SLOPE = .33;       // slope of entrance path before the curve
    const double STEER_GAIN = 1.0;      // gain of steering servo
    const double DAMP_GAIN = 1.2;       // damping of steering servo
    const double BIG_SLIP = 9.0;        // affects the bias of steering servo
    const double CURVE_END = 4.0;       // when you are near end of curve, widths
    const double TOO_FAST = 1.02;       // a ratio to determine if speed is OK in curve
    const double DELTA_LANE = 2.5;      // if collision predicted, change lane by this

    con_vec result;                     // This is what is returned.
    double alpha, vc;                   // components of result
    double bias;                        // added to servo's alpha result when entering curve
    double speed;                       // target speed for curve (next curve if straightaway)
    double speed_next;                  // target speed for next curve when in a curve, fps.
    double width;                       // track width, feet
    double to_end;                      // distance to end of present segment in feet.
    static double lane;                 // target distance from left wall, feet
    static double lane0;                // value of lane during early part of straightaway
    static int rad_was = 0;             // 0, 1, or -1 to indicate type of previous segment
    static double lane_inc = 0.0;       // an adjustment to "lane", for passing



    if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc))
      return result;

    width = s.to_lft + s.to_rgt;   // compute width of track

    // This is a little trick so that the car will not try to change lanes
    // during the "dragout" at the start of the race.  We set "lane" to
    // whatever position we have been placed by the host.
    if(s.starting)                // will be true only once
      lane = lane0 = s.to_lft;    // better not to change lanes during "dragout"

    // Set "lane" during curves.  This robot sets "lane" during curves to
    // try to maintain a small fixed distance to the inner rail.
    if(s.cur_rad > 0.0)        // turning left
    {
      lane = MARGIN;
      rad_was = 1;             // set this appropriate to curve.
    }
    else if(s.cur_rad < 0.0)   // turning right
    {
      lane = width - MARGIN;
      rad_was = -1;            // set this appropriate to curve.
    }
    else                              // straightaway:
    {
      // We will let the car go down the straigtaway in whatever "lane" it
      // comes out of the turn.
      if(rad_was)            // If we just came out of a turn, then:
      {
        lane = s.to_lft;         // set "lane" where we are now.
        if(lane < .5 * width)    // but maybe push it a little more to right?
          lane += MARG2;         // (add MARG2 if we were to left of center)
        lane0 = lane;            // save a copy of the new "lane" value.
        rad_was = 0;             // set this appropriate to straightaway.
      }
      // This is for the transition from straight to left turn.  If we are
      // in a transition zone near the end of the straight, then set lane to
      // a linear function of s.to_end.  During this zone, "lane" will change
      // from "lane0" upon entering the zone to MARG2 upon reaching the end
      // of the straightaway.  ENT_SLOPE is the change in lane per change in
      // s.to_end.
      if(s.to_end < (lane0 - MARG2) / ENT_SLOPE)
        lane = MARG2 + ENT_SLOPE * s.to_end;
    }

    // set alpha:  (This line is the complete steering servo.)
    alpha = STEER_GAIN * (s.to_lft - lane)/width - DAMP_GAIN * s.vn/s.v/* + bias*/;

    // set vc:
    if(s.cur_rad == 0.0)              // If we are on a straightaway,
    {                                          // if we are far from the end,
      if(s.to_end > CritDist(s.v, speed, BRAKE_ACCEL))
         vc = s.v + 50.0;                           // pedal to the metal!
      else                      // otherwise, adjust speed for the coming turn:
      {
         if(s.v > TOO_FAST * speed)             // if we're a little too fast,
           vc = s.v - BRAKE_SLIP;                // brake hard.
         else if(s.v < speed/TOO_FAST)         // if we're a little too slow,
           vc = 1.1 * speed;          // accelerate hard.
         else                               // if we are very close to speed,
           vc = .5 * (s.v + speed);   // approach the speed gently.
      }
    }
    else       // This is when we are in a curve:  (seek correct speed)
    {
      // calculate distance to end of curve:
      if(s.cur_rad > 0.0)
        to_end = s.to_end * (s.cur_rad + MARGIN);
      else
        to_end = -s.to_end * (s.cur_rad - MARGIN);
      // compute required braking distance and compare:
      // This is to slow us down for then next curve, if necessary:
      if(to_end <= CritDist(s.v, speed_next, BRK_CRV_ACC))
        vc = s.v - BRK_CRV_SLIP;
      // but if there is a straight, or a faster curve next, then
      // we may want to accelerate:
      else if(to_end/width < CURVE_END && speed_next > speed)
        vc = .5 * (s.v + speed_next)/cos(alpha);
      else   // normally, just calculate vc to maintain speed in corner
        vc = .5 * (s.v + speed)/cos(alpha);
    }

    // Here we gradually reduce lane_inc to zero if no collision is predicted:
    int kount = 0;

    if(!kount)
      if(lane_inc > .1)
        lane_inc -= .5*DELTA_LANE;
      else if(lane_inc < -.001)
        lane_inc += .5*DELTA_LANE;

    // lane_inc represents an adjustment to the lane variable.  This is
    // accomplished by changing alpha an amount equal to that which the
    // steering servo would have done had lane actually been changed.
    result.vc = vc;   result.alpha = alpha - STEER_GAIN * lane_inc / width;



//        con_vec result = CON_VEC_EMPTY;
        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot03Instance()
{
    return new Robot03();
}

